jQuery(document).ready(function($){
	var $spMenu = $('#sp-menu'),
			$nav = $('.nav');
	$spMenu.on('click', function(event){
		var $slideTarget = $nav.find('ul');
		$slideTarget.animate({ height: 'toggle', opacity: 'toggle' }, {duration: 1000, easing: 'easeInOutCubic', complete: function() {
				if( $slideTarget.css('display') == 'none' ) {
					$spMenu.find('.icon-remove').removeClass().addClass('icon-align-justify');
				} else {
					$spMenu.find('.icon-align-justify').removeClass().addClass('icon-remove');
				}
			}
		});
	});
});

// $(window).on('load',function(){
window.addEventListener('load', function(){
	$('a[href^="#"]').on('click', function(event) {
		var id = $(this).attr('href'),
				offset = 0,
				target = $(id).offset().top - offset;
		$('html, body').animate({scrollTop:target}, {duration: 1000, easing: 'easeInOutCubic'});
		event.preventDefault();
		return false;
	});
});