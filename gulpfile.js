var cssTimeStamp = require('gulp-timestamp-css-url');

var gulp = require('gulp'),
	fs = require('fs'),
	path = require('path'),
	$ = require('gulp-load-plugins')({
		pattern: ['gulp-*', 'gulp.*'],
		replaceString: /\bgulp[\-.]/
	}),
	browserSync = require('browser-sync');

/** 開発用のディレクトリを指定します。*/
var src = {
	'html': ['src/pug/**/*.pug', '!' + 'src/pug/**/_*.pug'], // 出力対象は`_`で始まっていない`.pug`ファイル。
	'json': 'src/pug/_data/', // JSONファイルのディレクトリを変数化。
};

/** 出力するディレクトリを指定します。*/
var dist = {
	'root': 'dist/',
	'html': 'dist/'
};

/** ローカルサーバーを起動します。*/
gulp.task('browser-sync', function() {
	browserSync.init(null, {
		server: {
			baseDir: dist.root
		},
		notify: true
	});
});
/** `.pug`をコンパイルしてから、distディレクトリに出力します。JSONの読み込み、ルート相対パス、Pugの整形に対応しています。*/
gulp.task('html', function() {
	var locals = { // JSONファイルの読み込み。
		'dashboard': JSON.parse(fs.readFileSync(src.json + 'dashboard.json'))
	}
	return gulp.src(src.html)
	.pipe($.plumber({errorHandler: $.notify.onError("Error: <%= error.message %>")})) // コンパイルエラーを通知します。
	.pipe($.data(function(file) { // 各ページごとの`/`を除いたルート相対パスを取得します。
		locals.relativePath = path.relative(file.base, file.path.replace(/.pug$/, '.html'));
			return locals;
	}))
	.pipe($.pug({
		locals: locals, // JSONファイルとルート相対パスの情報を渡します。
		basedir: 'src/pug', // Pugファイルのルートディレクトリを指定します。
		pretty: true // Pugファイルの整形。
	}))
	.pipe($.outdent())
	.pipe(gulp.dest(dist.html))
	.pipe(browserSync.reload({
		stream: true
	}));
});

gulp.task('styles', function() {
	return gulp.src('src/scss/*.scss')
	.pipe($.plumber({errorHandler: $.notify.onError("Error: <%= error.message %>")}))
		.pipe($.sass({
			outputStyle: 'compressed'
		}))
		.pipe($.csso())
		.pipe(cssTimeStamp({useDate:true}))
		.pipe(gulp.dest('dist/css'))
		.pipe(browserSync.reload({
			stream: true,
			once: true
		}));
});

/** Pugのコンパイル・CSSの出力、browser-syncのリアルタイムプレビューを実行します。*/
gulp.task('watch', ['html', 'styles', 'browser-sync'], function() {
	gulp.watch(['src/pug/**'], ['html']);
	gulp.watch(['src/scss/**'], ['styles']);
});

gulp.task('default', ['watch']);
