# pug-yarn

Gulpを使用したPugのリポジトリです。

## インストール
開発に必要なパッケージなどは以下のコマンドですべてインストールされます。

---
yarn
---

## 確認環境

以下の環境で動作の確認をしています。

- OS X 10.12.6(Sierra)
- Node.js 4.2.4

## ファイル構成
開発は'/src'ディレクトリでおこない、'/dist'ディレクトリに出力されます。

---
.
├──README.md
├──gulpfile.js
├──package.json
└── src/
	├──pug/
	│	├──_data/
	│	│  └── dashboard.json
	│	├──_includes/
	│	│  ├──_footer.pug
	│	│  ├──_header.pug
	│	│  ├──_layout.pug
	│	│  ├──_meta.pug
	│	│  └──_script.pug
	│	│  └──_script-ga.pug
	│	│  └──_script-social.pug
	│	└── index.pug
	│	└── page.pug
	├──scss/
---

## 開発タスク
以下のコマンドを実行すると、開発に必要なGulpのタスクがすべて実行されます。

---
yarn start
---

- pugをhtmlにコンパイル
- scssを'/dist/css/'にコンパイル
- ブラウザが起動して変更をリアルタイムに反映

## ngrok

### ngrokをインストール
---
brew cask install ngrok
---

### ngrokを起動
---
ngrok http 3000
---

- ローカル環境へ外部からアクセス